# Index
- [Roles](#roles)
    - [Guest](#guest)
    - [Reporter](#reporter)
    - [Developer](#developer)
    - [Manteiner](#manteiner)

# Roles

## Guest

- No permite realizar push a la rama master
---
![img](imagenes/noPushGuest.png)
- Ni con gitKraken
---
![img](imagenes/NoRamaRolesPush.png)
- No permite crear otras ramas
---
![img](imagenes/NoSePuedeCrearRamaGuest.png)
---
- No permite editar, ni crear labels
- Pero se puede clonar el proyecto 
- Permite crear Issues
- Se puede ver los MergeRequest
---
![img](imagenes/crearIssueGuest.png)



## Reporter
- No puedo crear Ramas
- No se puede crear archivos
- No se permite Editar, Reemplezar, ni Eliminar
- No se puede hacer Push
- Se puede ver los MergeRequest
- Se puede crear Issues
- Crear labels


## Developer

- No permite realizar push a la rama master
![img](imagenes/nopush.PNG)
- Permite crear ramas nuevas y realizar push 
- Permite Crear Merge Request
![img2](imagenes/merge.PNG)
- Permite ver los Merge Request
![img3](imagenes/mergerequest.PNG)
- Permite crear nuevos Issues
![img4](imagenes/issues.PNG)
- Permite crear nuevos Labels
![img5](imagenes/newlabel.PNG)

## Manteiner 
Permite realizar todo lo que permite el rol de Developer y además:
- Permite realizar push de cambios realizados en la rama master
![img6](imagenes/manteinerpushmaster.PNG)
- Permite añadir nuevos miembros
![img7](imagenes/addmember.PNG)
([inicio](#index))
